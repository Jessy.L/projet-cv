//______________________________________________
//______________________________________________
// lien de l'api
// https://www.prevision-meteo.ch/services/json/
//______________________________________________


const apiUrl = "https://www.prevision-meteo.ch/services/json/"

// Array qui va être utilisé avec les données recup de l'api pour la function meteo
var info_array_text = [
    "Nom de la ville : ",
    "Levé du Soleil : ",
    "Couché du soleil : ",
    
    "Temp actuelle °c : ",
    "Vitesse du vent KM/H : ",
    "Rafale de vent KM/H : ",
    'Condition : ',
    "Pression atmosphérique (hPa) : ",
    "Humidité  % : ",

    "Jour : ",
    "Date : ",
    "Temp MIN °c : ",
    "Temp MAX °c : ",

]

// Array qui va être utilisé avec les données recup de l'api pour la function autreJour
var jour_info_texte = [
    "Jour : ",
    "Date : ",
    "Condition : ",
    "Temp MIN °c: ",
    "Temp MAX °c: ",
]

// div content 
var info_ville_div = document.getElementById('info_ville')
var autre_jour_div = document.getElementById('autre-jour')

// input + button + select(et label)
var ville = document.getElementById('ville')
var envoyer = document.getElementById('envoyer').addEventListener('click', meteo) 
var label_autre_jour_select = document.getElementById('label-autre-jour-select')
var select = document.getElementById('autre-jour-select')

// rend invisible select et le label
label_autre_jour_select.style.display= 'none'
select.style.display = 'none'


function meteo(){
    // vide la div info ville
    info_ville_div.innerHTML = ""
    
    // Crée un lien d'envoi pour la requête API 
    let url = apiUrl + ville.value
    
    // Envoi de la requête et recup format JSON
    fetch(url, {method: 'get'}).then(response => response.json()).then(results => {
        
        // Crée un array avec toute les données qu'on souhaite recup
        var info_array = [
            
            results.city_info.name,
            results.city_info.sunrise,
            results.city_info.sunset,
            
            results.current_condition.tmp,
            results.current_condition.wnd_spd,
            results.current_condition.wnd_gust,
            results.current_condition.condition,
            results.current_condition.pressure,
            results.current_condition.humidity,
            
            results.fcst_day_0.day_long,
            results.fcst_day_0.date,
            results.fcst_day_0.tmin,
            results.fcst_day_0.tmax,

            
        ]
        
        // Recup l'image de l'api et la crée en HTML
        img = document.createElement('img')
        img.src = results.current_condition.icon_big
        info_ville_div.appendChild(img)
        
        // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
        for(i = 0; i < info_array.length; i++){
            var p = document.createElement('p')
            p.innerHTML = info_array_text[i] +  "<span>" + info_array[i] + "</span>"
            info_ville_div.appendChild(p)
        }
        
        // Récup les jours de l'api
        var jour = [
            results.fcst_day_0,
            results.fcst_day_1,
            results.fcst_day_2,
            results.fcst_day_3,
            results.fcst_day_4
        ]

        // Affiche le select et le label
        select.style.display = ''
        label_autre_jour_select.style.display = ""
        select.addEventListener('change', autreJour);

        // Crée les options pour le select avec les 4 jours qui suivent 
        for(i = 0; i < 5; i++){
            var option = document.createElement('option')
            option.value = "results.fcst_day_"+i
            option.innerHTML = jour[i].day_long
            select.appendChild(option)
        }


    }).catch(err => {
        console.error(err)

        // en cas d'erreur affiche un message dans la div info_ville_div
        var p = document.createElement('p')
        p.innerHTML = 'Nom de la ville incorrect ou hors Europe'
        p.style.color = 'red'
        info_ville_div.appendChild(p)
    })

}


function autreJour(){
    
    // vide la div autre_jour_div
    autre_jour_div.innerHTML = ""

    // Crée l'url pour la requête vers l'api
    let url = apiUrl + ville.value

    // La requête qui récupère les données en JSON
    fetch(url, {method: 'get'}).then(response => response.json()).then(results => {

        // Crée l'image en HTML 
        var img = document.createElement('img')
    
        // Si le select == à la condition alors  
        if(select.value == "results.fcst_day_0"){

            // Crée un tableau avec les données qu'ont souhaite afficher 
            var jour_info = [
                results.fcst_day_0.day_long,
                results.fcst_day_0.date,
                results.fcst_day_0.condition,
                results.fcst_day_0.tmin,
                results.fcst_day_0.tmax
            ]
            
            // rajoute le lien à l'img et l'attache au parent
            img.src = results.fcst_day_0.icon_big
            autre_jour_div.appendChild(img)
    
            // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
            for(i = 0; i < jour_info.length; i++){
                var p = document.createElement('p')
                p.innerHTML = jour_info_texte[i] +  "<span>" + jour_info[i] + "</span>"
                autre_jour_div.appendChild(p)
            }
            
        // Ou bien le select == à la condition alors  
        }else if(select.value == "results.fcst_day_1"){

            // Crée un tableau avec les données qu'ont souhaite afficher 
            var jour_info = [
                results.fcst_day_1.day_long,
                results.fcst_day_1.date,
                results.fcst_day_1.condition,
                results.fcst_day_1.tmin,
                results.fcst_day_1.tmax
            ]
            
            // rajoute le lien à l'img et l'attache au parent
            img.src = results.fcst_day_1.icon_big
            autre_jour_div.appendChild(img)
    
            // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
            for(i = 0; i < jour_info.length; i++){
                var p = document.createElement('p')
                p.innerHTML = jour_info_texte[i] +  "<span>" + jour_info[i] + "</span>"
                autre_jour_div.appendChild(p)
            }

        // Ou bien le select == à la condition alors              
        }else if(select.value == "results.fcst_day_2"){

            // Crée un tableau avec les données qu'ont souhaite afficher 
            var jour_info = [
                results.fcst_day_2.day_long,
                results.fcst_day_2.date,
                results.fcst_day_2.condition,
                results.fcst_day_2.tmin,
                results.fcst_day_2.tmax
            ]
            
            // rajoute le lien à l'img et l'attache au parent
            img.src = results.fcst_day_2.icon_big
            autre_jour_div.appendChild(img)
    
            // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
            for(i = 0; i < jour_info.length; i++){
                var p = document.createElement('p')
                p.innerHTML = jour_info_texte[i] +  "<span>" + jour_info[i] + "</span>"
                autre_jour_div.appendChild(p)
            }
            
        // Ou bien le select == à la condition alors  
        }else if(select.value == "results.fcst_day_3"){

            // Crée un tableau avec les données qu'ont souhaite afficher 
            var jour_info = [
                results.fcst_day_3.day_long,
                results.fcst_day_3.date,
                results.fcst_day_3.condition,
                results.fcst_day_3.tmin,
                results.fcst_day_3.tmax
            ]
            
            // rajoute le lien à l'img et l'attache au parent
            img.src = results.fcst_day_3.icon_big
            autre_jour_div.appendChild(img)
    
            // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
            for(i = 0; i < jour_info.length; i++){
                var p = document.createElement('p')
                p.innerHTML = jour_info_texte[i] +  "<span>" + jour_info[i] + "</span>"
                autre_jour_div.appendChild(p)
            }

        // Ou bien le select == à la condition alors  
        }else if(select.value == "results.fcst_day_4"){

            // Crée un tableau avec les données qu'ont souhaite afficher 
            var jour_info = [
                results.fcst_day_4.day_long,
                results.fcst_day_4.date,
                results.fcst_day_4.condition,
                results.fcst_day_4.tmin,
                results.fcst_day_4.tmax
            ]
            
            // rajoute le lien à l'img et l'attache au parent
            img.src = results.fcst_day_4.icon_big
            autre_jour_div.appendChild(img)
    
            // Boucle qui recupère toute les données dans le info_array et la convertie en HTML
            for(i = 0; i < jour_info.length; i++){
                var p = document.createElement('p')
                p.innerHTML = jour_info_texte[i] +  "<span>" + jour_info[i] + "</span>"
                autre_jour_div.appendChild(p)
            }

        }else{
            // affiche un message d'erreur dans la console 
            console.error('SELECT EDIT OR VALUE UNDEFINED')
        }
    }).catch(err => {
        // affiche un message d'erreur dans la console avec l'erreur de la requête 
        console.error(err)
    })
}
