
var input = document.getElementById('nombre')
var button = document.getElementById('button_verif').addEventListener('click', verif)
var result_div = document.getElementById('result')

// var chaine = "972487086";

function verif(){
    
    result_div.innerHTML = ""
    
    var chaine = String(input.value)
    var original = input.value
    chaine = chaine.split("").reverse()
    let compte = 1
    
    for(i = 0; i < chaine.length; i++){
                
        var mult = chaine[Number(compte)] * 2

        if(compte < chaine.length){
            if(mult > 9){
        
                mult = String(mult).split("")
                let calc = (Number(mult[0]) +  Number(mult[1]))
                chaine.splice(compte, 1 , calc )
            
            } else if(compte%2 !== 0){
            
                chaine.splice(compte, 1 , mult )
            
            }
        } else { 
            break
        }
        compte = compte + 2
    }
    
    calcul(chaine , original)
}

function calcul(chaine, original){

    var stock = ""

    for( i = 0 ; i < chaine.length ; i++){
    
        stock = Number(stock) + Number(chaine[i])
       
    }

    chaine.reverse()

    var p = document.createElement('p')
    p.innerHTML = 'Le nombre a vérifier : ' + original.split('')
    result_div.appendChild(p)

    var p = document.createElement('p')
    p.innerHTML = "Application de l'algo : " + chaine.reverse().join()
    result_div.appendChild(p)

    if(stock%10 == 0 ){

        var p = document.createElement('p')
        p.innerHTML = 'La somme : ' + stock
        result_div.appendChild(p)

        var p = document.createElement('p')
        p.innerHTML = 'La somme %10 = ' + stock%10 + " donc valide"
        result_div.appendChild(p)

    }else{

        var p = document.createElement('p')
        p.innerHTML = 'La somme : ' + stock
        result_div.appendChild(p)

        var p = document.createElement('p')
        p.innerHTML = 'La somme %10 = ' + stock%10 + " donc invalide"
        result_div.appendChild(p)

    }

    
}
