var button_voir_plus = document.getElementById('button-voir-plus')
button_voir_plus.addEventListener('click', block)

var block_div = document.getElementById('block')

function block(){

    if (block_div.style.display == 'none'){
         block_div.style.display = 'flex';
         button_voir_plus.value = "Afficher moins"
    }
    
    else{
         block_div.style.display = 'none';
         button_voir_plus.value = "Afficher plus"
    }

}

block()